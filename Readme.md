Шпаргалка по командам Docker 
-------------------------------

### Просмотр имеющихся image's
`docker images -a`

### Удалить все image
`docker rmi $(docker images -a -q) -f`

### Запуск контейнера через docker-compose 
`docker-compose up -d`  
Если надо билдить за новог добавляем `--build`  
Если хотим видеть вывод в окнсоль убираем `-d`   

### Остановка через docker-compose 
`docker-compose down`

### Посмотреть текущие запущенные контейнеры 
`docker-compose down`
или
`docker ps`

### Посмотреть список созданных контейнеров  
`docker container ls -a`  

### Удалить все объекты докера  
`docker system prune`  